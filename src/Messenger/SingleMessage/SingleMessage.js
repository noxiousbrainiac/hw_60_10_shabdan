import React from 'react';
import '../../assets/bootstrap.min.css';

const SingleMessage = ({text, author, time}) => {
    return (
        <p className="card p-1">
            <span>
                <b>{author}</b> : {time}
            </span>
            <span>{text}</span>
        </p>
    );
};

export default SingleMessage;