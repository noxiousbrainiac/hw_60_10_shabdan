import React from 'react';
import SingleMessage from "../SingleMessage/SingleMessage";

const ReadMessages = ({messages}) => {
    return (
        <div className="p-3">
            {messages.map((mes, index) => (
                <SingleMessage key={index} author={mes.author} text={mes.message} time={mes.datetime}/>
            ))}
        </div>
    );
};

export default ReadMessages;