import React, {useCallback, useEffect, useState} from 'react';
import axios from "axios";
import MessengerForm from "./MessengerForm/MessengerForm";
import ReadMessages from "./ReadMessages/ReadMessages";

const url = 'http://146.185.154.90:8000/messages';

const Messenger = () => {
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState("");
    const [input, setInput] = useState("");

    const postAxios = async () => {
        const data = new URLSearchParams();

        if (message.length > 0){
            data.set('message', message);
        }

        if (input.length > 0){
            data.set('author', input);
        }

        await axios.post(url, data);
        await getAxios();
        setMessage("");
        setInput("");
    }

    const getAxios = async () => {
        const {data} = await axios.get(url);
        setMessages(data);
    }

    const intervalAxios = useCallback(async () => {
        const {data} = await axios.get(url + "?datetime=" + messages[messages.length - 1]?.datetime);
        setMessages(messages.concat(data));
    }, [messages]);

    useEffect(() => {
        getAxios();
    },[]);

    useEffect(() => {
        if (messages.length !== 0) {
            const interval = setInterval(() => {
                intervalAxios();
            }, 2000);

            return () => clearInterval(interval);
        }
    },[messages, intervalAxios]);

    return (
        <div className="container p-4">
            <ReadMessages messages={messages}/>
            <MessengerForm
                onPost={postAxios}
                input={input}
                setInput={setInput}
                message={message}
                setMessage={setMessage}
            />
        </div>
    );
};

export default Messenger;