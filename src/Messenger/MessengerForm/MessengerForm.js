import React from 'react';
import '../../assets/bootstrap.min.css';

const MessengerForm = ({onPost, message, input, setInput, setMessage}) => {
    return (
        <div>
            <div className="mb-3">
                <label htmlFor="exampleFormControlInput1" className="form-label">Username</label>
                <input
                    type="text"
                    className="form-control"
                    onChange={e => setInput(e.target.value)}
                    value={input}
                />
            </div>
            <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Message</label>
                <textarea
                    className="form-control"
                    rows="3"
                    style={{height: "150px" ,resize: "none"}}
                    onChange={e => setMessage(e.target.value)}
                    value={message}
                >
                </textarea>
            </div>
            <button
                className="btn btn-primary"
                type="button"
                onClick={() => onPost()}
            >
                Send
            </button>
        </div>
    );
};

export default MessengerForm;